-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 10, 2018 at 06:22 AM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stackpost`
--

-- --------------------------------------------------------

--
-- Table structure for table `facebook_accounts`
--

CREATE TABLE `facebook_accounts` (
  `id` int(11) NOT NULL,
  `ids` text,
  `uid` int(11) DEFAULT NULL,
  `pid` text,
  `fbapp` text,
  `fullname` text,
  `avatar` text,
  `access_token` text,
  `status` int(1) DEFAULT '1',
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facebook_accounts`
--

INSERT INTO `facebook_accounts` (`id`, `ids`, `uid`, `pid`, `fbapp`, `fullname`, `avatar`, `access_token`, `status`, `changed`, `created`) VALUES
(1, '8bc0f54f73002e44f237873186d2a2e9', 38, '100003294487381', 'Facebook for iPhone', 'Generasi Islam Muda', 'https://graph.facebook.com/100003294487381/picture', 'EAAAAAYsX7TsBAOM5RFwoDbbdv5ly7nPMi7vvwynvidCz8if3uFsPBZBoa1ZArpIF0gcgiHZCe1dJ1DyxfbF76ZAL4fx9HWNIuRmST98mKhrY9TqZAZBVmeZCuXeHId2vEnkElsaxajNZAfAmCZBAqWZCQISShZAfsXjaPbxoeQ58rlANNI5vWoZAYVCkOZCGh6VqkugckSbM7lDqNmOBqxGoTgEMZB', 1, '2018-11-06 21:34:56', '2018-11-06 21:34:56');

-- --------------------------------------------------------

--
-- Table structure for table `facebook_groups`
--

CREATE TABLE `facebook_groups` (
  `id` int(11) NOT NULL,
  `ids` text,
  `uid` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `type` text,
  `category` text,
  `pid` text,
  `name` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facebook_groups`
--

INSERT INTO `facebook_groups` (`id`, `ids`, `uid`, `account`, `type`, `category`, `pid`, `name`) VALUES
(1, 'c65e4b97e0a933c6c685185674ebb8ca', 38, 1, 'group', 'SECRET', '704997209576534', 'Fata Ganteng I love you Muah :* ,, aku nge fans berat sama kamu Faatt'),
(2, '273f43298af4c1b94089236b7a55b4c8', 38, 1, 'group', 'SECRET', '521758724607338', 'My WEB'),
(3, '7a89426979356077356c9d2fdf047eb4', 38, 1, 'group', 'CLOSED', '1503142636659206', 'LINE Webtoon Indonesia'),
(4, '4f7e52a159864a70ea5bdabfd5b519c4', 38, 1, 'group', 'OPEN', '655132094563533', 'SONG AND SING'),
(5, 'b9b83f532618769a5f1c68a1adeda627', 38, 1, 'group', 'CLOSED', '377826932328965', 'Curahan Perasaan dan Humor'),
(6, '7937da9b45299e959c6096b4fc6d90d3', 38, 1, 'group', 'OPEN', '258229904313878', '♣ Santri Blogger ♣'),
(7, '23a5ad3f5725737e94da9a821fe68ea5', 38, 1, 'group', 'SECRET', '495003573893946', 'Kebo :D'),
(8, 'd0239a4c10478d24e76eae82f6e46e39', 38, 1, 'group', 'SECRET', '194368140688127', 'Facebook'),
(9, '580dbb498657df8bc40810a3d9982d3c', 38, 1, 'group', 'CLOSED', '107849976046877', 'Dzakira Talita Zahra'),
(10, '0c5b3e83ecb5e30c290d0b41b8067177', 38, 1, 'group', 'CLOSED', '303918443056218', 'GARSIDA (GAbungan Remaja SIswa Delapan A)'),
(11, 'e81b57f408d512a40a93a18c697d2492', 38, 1, 'group', 'CLOSED', '501781859856160', 'Rafiz Cah Boementzz'),
(12, 'bba75999abe251bbc60ca8a2045e58a8', 38, 1, 'group', 'CLOSED', '455365654516450', 'Rafiz Cah Boementzz'),
(13, '7abbeacd24573fde2d7a8f5d414e57d9', 38, 1, 'group', 'CLOSED', '358549097568809', 'Rafiz Cah Boementzz'),
(14, '6d77065ceb1626d3d5ae05c2e0571a6e', 38, 1, 'group', 'CLOSED', '342653342498474', 'Rafiz Cah Boementzz'),
(15, 'a0ccf40b228dfd391ebbed19fa9f714f', 38, 1, 'group', 'CLOSED', '272009639586804', 'Click Me'),
(16, '0f81d0c5b26208a2bdeb5ca6e257402c', 38, 1, 'group', 'SECRET', '314715098624790', 'Queen.Net'),
(17, '118346936e26aac35b3548f8bc6cc284', 38, 1, 'group', 'OPEN', '450940478283560', 'Gr0up RaMe-RaMe'),
(18, '5bc0e225cc06e1d7105f6abd461be470', 38, 1, 'group', 'CLOSED', '420613334632703', 'Alumni sd 3sktn sang penguasa sukatani'),
(19, '4059ee3b9a5c18798edfbab8ab56a1c7', 38, 1, 'group', 'OPEN', '206103522826383', 'ilmu ku ,cahayaku'),
(20, '7a89f7fb495719edfa99bd18cbaa0f4a', 38, 1, 'group', 'CLOSED', '185522928136919', '\'\'b4tuR4n\'\' ......   ??!'),
(21, '4b747036068e681f3d94ce66625dd99b', 38, 1, 'friend', 'male', '1001091955', 'Bhadelz Khaira'),
(22, '2de1bd843de5b512d89bde6e07167951', 38, 1, 'friend', 'female', '1413058070', 'Nida Fauziyah'),
(23, 'abddc2c08aa7ab11cbb039431a3dae34', 38, 1, 'friend', 'male', '1421933499', 'Uxep Rahayu Mulyana'),
(24, '3c5f2b3dcc7bdf4e4707d4fd223acb62', 38, 1, 'friend', 'female', '1470399536', 'Ghina Aghniya'),
(25, 'f1970b4199363f4a4c11afe04319a97f', 38, 1, 'friend', 'male', '1478015454', 'Riski Fadilah'),
(26, 'bda2e49c195bc3c89e476d6fa5f92a3f', 38, 1, 'friend', 'female', '1517444260', 'Ira Nurmala Sari'),
(27, '751f8aa1e5e3aec46959573f7ae6f85c', 38, 1, 'friend', 'female', '1565605878', 'Syifa Hayatunnisa'),
(28, '7977398fbe6fc29d7b749fc970b521da', 38, 1, 'friend', 'female', '1677825070', 'Rd Anggia M Nuraini'),
(29, '373e50652ff18322d8c0badc07bf8df6', 38, 1, 'friend', 'female', '1678812299', 'Reni Yulianti'),
(30, '4172478a744e2d13cbda5314c55a7bb3', 38, 1, 'friend', 'female', '1687140965', 'Athun'),
(31, 'fa9943440b4b3b60f724887494c4d8fc', 38, 1, 'friend', 'female', '1795782046', 'Sophia Fitriani'),
(32, '992d1e027dc6dca3ceb1cb06f4cb9da0', 38, 1, 'friend', 'male', '1845664250', 'Ghazian R Kasyfillah'),
(33, '19e47b9fa5eea9c61b33288aae64c3a5', 38, 1, 'friend', 'female', '100000007793278', 'Shifa Zeni'),
(34, 'ef459fbfd5208c7756c8642f45087f4e', 38, 1, 'friend', '', '100000013704874', 'Hilda Fauziyah'),
(35, '958897e3f8e0baa7b731e1bd81f19298', 38, 1, 'friend', '', '100000105801177', 'Fifi Nurshifa Budiarti'),
(36, 'af0a610facbeab099bc09c86ff368a1a', 38, 1, 'friend', 'male', '100000130307890', 'Hanif Basalamah Elbas'),
(37, '51db71ff078ced37e5690e0e12b3ba7a', 38, 1, 'friend', 'male', '100000193045344', 'Saeful Rickiardy'),
(38, '5fce8bf5916dfcd448d10ece731d1216', 38, 1, 'friend', 'male', '100000212730488', 'Bima Latuconsina'),
(39, 'eb6d312ae2b09a2dfbfcaa5013cd884c', 38, 1, 'friend', 'female', '100000253275392', 'Young Hanggeng S\'junior'),
(40, '1d56f1b2d103a87dabb561f3e08d02ef', 38, 1, 'friend', 'female', '100000257034987', 'Hilda Like Candy'),
(41, '67ceed712152909d5cf5b527c85b5d28', 38, 1, 'friend', 'male', '100000312080864', 'Shogi Basahiel'),
(42, '99ace43f86fb35084af3d35821b7d490', 38, 1, 'friend', 'male', '100000344730704', 'Markatio Tio'),
(43, '02bd920a9be90dea1d1d44ff3759bf62', 38, 1, 'friend', 'female', '100000347350024', 'Cherry Kim Xiaosi Khielz'),
(44, '201f8e14eb30676c959ff5925fdf8321', 38, 1, 'friend', 'male', '100000348507334', 'Ahmad Jimad Madun'),
(45, '4fd15eeefa125d2a1e1233d2cd54efcf', 38, 1, 'friend', 'female', '100000392148784', 'Heni Rohaeni'),
(46, '77ae10e78a088fce1adf5e15392801ce', 38, 1, 'friend', '', '100000411626424', 'Rosa Safa\'atillah'),
(47, 'a0f850917af37d53d587725178c44c4f', 38, 1, 'friend', '', '100000429919270', 'Fadlan Fadlani'),
(48, '9c3466323f039bcafc45b7312503d61e', 38, 1, 'friend', 'male', '100000443125013', 'Kiki Ramdani'),
(49, 'ba737c5c5714c618c66898c6f5255f4b', 38, 1, 'friend', '', '100000448916179', 'Yusuf Wildani Fauzi'),
(50, '41e3a8a9f5da3453fc4d71dbc4489974', 38, 1, 'friend', 'male', '100000450393348', 'Eki Muhammad Fauzan'),
(51, '47bebf1ccac127e610ceff0125cd3fa5', 38, 1, 'friend', 'male', '100000453804719', 'Hisam Ahmad'),
(52, '6f36606af5abdfd03546c418ea014f58', 38, 1, 'friend', 'male', '100000457243931', 'Dea Arsyad Mujtahid Shibghotulloh'),
(53, '1a76132b26fa913d020fb5c68a54bc31', 38, 1, 'friend', 'female', '100000460691661', 'Aulia Azizah Ulfah'),
(54, '17061e985a952b95ba485da46e57b373', 38, 1, 'friend', 'female', '100000478804242', 'Shilvana Bajrie'),
(55, 'a4fcbdd488ad6a3110f1d6961a8186de', 38, 1, 'friend', 'female', '100000479941803', 'Putri Utami'),
(56, 'b1a4ae4c9b5690b74a1db7d5c55c08a4', 38, 1, 'friend', 'female', '100000504954845', 'Bella Kartika'),
(57, '667cfe8a7bb643d9c537832eabefc825', 38, 1, 'friend', 'female', '100000508020706', 'Hanifah Hutari'),
(58, 'fde9595375efa5e9ee8031651aabe2dc', 38, 1, 'friend', 'female', '100000510433437', 'Fitraini Bayyinah'),
(59, '1e0ea01bc13fa0c9d224c848f66839ae', 38, 1, 'friend', 'male', '100000513540624', 'Rizal Azis'),
(60, '32c24c9bffbd651f7c3555e296d2f523', 38, 1, 'friend', 'female', '100000516578157', 'Alfi Putri'),
(61, '919e09cc638d5bf221b53c0066cbc589', 38, 1, 'friend', 'female', '100000521791953', 'Nerestia'),
(62, '9c62d1e98bf0bbc76808945315207ee6', 38, 1, 'friend', '', '100000533347500', 'Evi Sopiah'),
(63, 'e6b1d904eee7367cc2977356741280df', 38, 1, 'friend', '', '100000539268393', 'Reni Nuraeni'),
(64, 'ab1673ad8d9ed6d70cd6f7d563812996', 38, 1, 'friend', 'male', '100000543646978', 'Asep Saeful Iman'),
(65, '38369e40274116d1ea55f15c863da265', 38, 1, 'friend', 'female', '100000545800693', 'Sheila Shania Salsabila'),
(66, '310e1c3b55656f4446aea9233e58c1a0', 38, 1, 'friend', 'female', '100000574125850', 'Ulya Nurwitri Ramadanti'),
(67, '5531e8d7851eeab66e78169bc932c5a9', 38, 1, 'friend', 'male', '100000595580318', 'عامر باجري'),
(68, '3651fec0232364dc46357d281bad35fb', 38, 1, 'friend', 'female', '100000600104326', 'Inggit Sri Wahyuni Kurniawan'),
(69, '3d9f55d7a5a7f64a83a3bef8c3cf14ee', 38, 1, 'friend', 'male', '100000607322368', 'Hisam Ahmad'),
(70, '5ec8d9ea70623827f80ab87b4dbfbbf1', 38, 1, 'friend', 'male', '100000631528653', 'Hanif Mujaddid Al Adzkiya'),
(71, 'f88336a125201b04e472e9729555e96d', 38, 1, 'friend', 'female', '100000656472832', 'Lusi Oktaviani'),
(72, '5ccefa18730f5249edff11eea52b8fee', 38, 1, 'friend', 'female', '100000700973012', 'Diannisa Aprilla'),
(73, 'be8cbb7e02af03ea74e9fadceb7e7997', 38, 1, 'friend', 'female', '100000704934279', 'Fan Fan Cie Spentyy'),
(74, '4ee3fe9eca8ca1b84265d55d652b963f', 38, 1, 'friend', 'male', '100000706836505', 'Fata El Islami'),
(75, '045dbf0eb7a0120be824892039550ece', 38, 1, 'friend', 'female', '100000721049184', 'Solihatun Nihayyah'),
(76, '63830721af99ee0b77de917c42552a04', 38, 1, 'friend', 'female', '100000738461002', 'KhairunNisa Oktafiani'),
(77, '25e771006bebc4839df9073fb30e6947', 38, 1, 'friend', 'male', '100000741882084', 'Ayodhyoa'),
(78, 'b76c06f4a0090e49a88e0196dd73bea9', 38, 1, 'friend', 'male', '100000743067706', 'Aripin Ipank'),
(79, '6e04747856b5ad036dc48d7f42cce9b5', 38, 1, 'friend', 'female', '100000795487338', 'Nurendah Etika Sari'),
(80, 'ec7c19a3594f5c15cbc80278f6c4df24', 38, 1, 'friend', '', '100000843056478', 'Deborah Mamesa'),
(81, '0edfcfab704d972b110e80d664166f07', 38, 1, 'friend', 'female', '100000931991233', 'Angraeni Angra'),
(82, '194ed2aa5a132a5c78cd910826c2cab7', 38, 1, 'friend', 'female', '100000965015743', 'Izmi Ulfa Fauziah'),
(83, 'a8c056f4ba07e42ba112c9712a5997f7', 38, 1, 'friend', 'female', '100000993757883', 'Dea Fauziah'),
(84, '38a7cbbe884dcd4c29a2bef7ac9defa3', 38, 1, 'friend', 'female', '100001012627874', 'Rizqy Awalia Muharom'),
(85, 'a8a531e78188bc2e90798d75c51fe6af', 38, 1, 'friend', 'female', '100001055393763', 'Rintanti Puspita Dewi'),
(86, '8bda60337320c14689557510d708ea89', 38, 1, 'friend', '', '100001084903732', 'Iint Chechuint Treeshwhyna'),
(87, '5ee4b9bda6b41edee8c9ee9d1258c869', 38, 1, 'friend', 'female', '100001176163321', 'Yati Septyana'),
(88, 'f01081536aafef187bde9c725d73f997', 38, 1, 'friend', 'female', '100001206601974', 'Febi Fauziah'),
(89, 'a1da406992a017ed8248a3732a862a6b', 38, 1, 'friend', 'female', '100001219992555', 'Qonita Wilani'),
(90, '5b6d7e880ae586c1486bfc3a379e68f0', 38, 1, 'friend', 'male', '100001230801235', 'Moch Najib Wasi\'a'),
(91, '677b088825ce6374e6078772b7840e8d', 38, 1, 'friend', 'male', '100001234188515', 'Muhamad Alvi'),
(92, 'cedfb311852004e1418df597657bbe3e', 38, 1, 'friend', 'male', '100001249325150', 'Fadlan Fadlani Zaelani'),
(93, 'd281810dcfb7a1d5b8ff441867eed52a', 38, 1, 'friend', 'female', '100001377510419', 'Sarah Falistin'),
(94, '36e180fd6596f39a453de5bdba2c759d', 38, 1, 'friend', '', '100001389428887', 'Fahadz Nasrulloh'),
(95, '4aaf7ffd52d3fd79e0b29b61755f4623', 38, 1, 'friend', 'female', '100001389856280', 'Nurhasanah'),
(96, '0de0a709da7d33938083e4bda6380267', 38, 1, 'friend', 'female', '100001419845313', 'Tand Che Smash'),
(97, '580338b8796f337cb845622978983d36', 38, 1, 'friend', 'female', '100001423540025', 'Halimahh'),
(98, '014a69ccd2516c07faed8f8ee856b742', 38, 1, 'friend', 'male', '100001452449046', 'Rizky Dade'),
(99, '31f9a3fc97526318bf03e5c9ccd2a3e0', 38, 1, 'friend', '', '100001460033410', 'Hesti Resmiani'),
(100, 'c535f858cde1d9bd122afcc10b519349', 38, 1, 'friend', 'male', '100001460574941', 'Agung Iw'),
(101, 'b2e9f8a4e7de53368705db7ad3c6ebea', 38, 1, 'friend', 'female', '100001464816361', 'Ayu Afaf Al-kayla'),
(102, '02fa05515d1601964fd154a178276d56', 38, 1, 'friend', 'male', '100001502244791', 'Abdullah'),
(103, '8d5cccc91d65dab0e7f3c0ed5b0989be', 38, 1, 'friend', 'female', '100001564866209', 'Yunita Ajah'),
(104, 'd0d8fc7c98b2bae4e6cca3c38dae4108', 38, 1, 'friend', 'female', '100001576113868', 'Maryam Jamilah'),
(105, '04a841f1d5be91a7f129c374007edae5', 38, 1, 'friend', 'female', '100001583454385', 'Dhedot'),
(106, '7ba599a160d8eccdf853a58b31dd6eae', 38, 1, 'friend', 'female', '100001600078363', 'Dian Fitri Novianti'),
(107, '3ba06a36465d9978520e04ca871ed504', 38, 1, 'friend', 'female', '100001618101106', 'Nia Ratnasari'),
(108, 'c6ca65f3aa948eb41f6b7e107d3a240b', 38, 1, 'friend', 'male', '100001644183026', 'Fahmi Reynaldi'),
(109, '42ddf2eb322f1ab25225808f263ab15d', 38, 1, 'friend', 'female', '100001653350741', 'Hilda Damayanti'),
(110, '168e199ab2d7e52c9f435761aeeca286', 38, 1, 'friend', 'female', '100001661677701', 'Dede Elis'),
(111, 'db1502e65586357b1141416d73ec5067', 38, 1, 'friend', 'female', '100001668234403', 'Syifa Musyarofah Habsari'),
(112, '2fdce7b152aa36274db4b351eee3915d', 38, 1, 'friend', 'female', '100001674238125', 'Tina Gustiani'),
(113, '624509a9c49da2c4abdb0397593c7caf', 38, 1, 'friend', '', '100001677811938', 'Saula Hadyaa Hayati'),
(114, 'ead0010fdf1d942083d86ceb6ee026ce', 38, 1, 'friend', 'male', '100001719213089', 'Dhirga Maulana'),
(115, '2547731e45dc34aed811b435f2a89a6a', 38, 1, 'friend', 'female', '100001735417773', 'Fida Ahsana Kusmayadi'),
(116, 'ab9694f043dec4b9e2c527156f3ba795', 38, 1, 'friend', '', '100001765872734', 'Newperdankillingmeinside Sixoneninesimbriigidigloverzz Hardiansyahfey\'hard'),
(117, '72cece3dc0d357d2774d3ca172908021', 38, 1, 'friend', 'male', '100001804639038', 'Norman Sofyan'),
(118, '3a97acc846dbd8a52cea25f06f68fdd5', 38, 1, 'friend', 'female', '100001807344952', 'Putri Aprilianti'),
(119, '93ddd97ece9fac9bb5cb31a49448d0a9', 38, 1, 'friend', 'male', '100001863910341', 'Didier Alief'),
(120, '56af3d779278b48ca9c145ff757af793', 38, 1, 'friend', 'male', '100001878001610', 'Rey Black Devill'),
(121, '1d618660db3edbc46bf379eedea9c7b2', 38, 1, 'friend', 'male', '100001964231334', 'Muhammad Faizal Haq'),
(122, '725e3106db66a178939db0f575c16304', 38, 1, 'friend', 'male', '100001973128507', 'Chandra Kichin To Chita'),
(123, '1b42ddcf8af37f181dae647b9516c65d', 38, 1, 'friend', 'female', '100001980151135', 'Arfana Piz'),
(124, 'd99e79fe69723cd705fc72107d958b3c', 38, 1, 'friend', 'female', '100001981957115', 'Iqmaa Darussalam'),
(125, 'ef9c00eda7cd602ffd4b73844a2fbd20', 38, 1, 'friend', 'male', '100002047120993', 'Agiel Hanggara'),
(126, '16929642254c1268f5c717f7ae936969', 38, 1, 'friend', 'male', '100002048591037', 'Ilyasa Anam Andarustin'),
(127, '29f8be4d3d0dbea17ed5d9c179d072c2', 38, 1, 'friend', 'female', '100002089347286', 'Nurul Farhanah'),
(128, '107b9bb9a8b27387074f329df66b3e45', 38, 1, 'friend', 'male', '100002129161814', 'Abba Felisha'),
(129, '8162dfcd03459db18a2e21dbd367fe50', 38, 1, 'friend', 'female', '100002146249544', 'Vivi Fitriah'),
(130, '8af10c7b4dccac338a42f051573b8efd', 38, 1, 'friend', 'female', '100002187880526', 'Urffii Naufalia R'),
(131, '6ef200e8003cf4f25f76b76a4f2ce812', 38, 1, 'friend', 'female', '100002201114317', 'Irma Raihanah'),
(132, 'c9f66598086c25ea40bd715b99968bf1', 38, 1, 'friend', 'male', '100002211727015', 'Mohamad Zamzammy Clf'),
(133, 'c80129f903fc270071598fbd9eec5a5c', 38, 1, 'friend', '', '100002229835922', 'ᎠᎡ ᎻᎪᏟᏦᎬᏒ'),
(134, '15032e3af18b3bb1c361ece61e2666c3', 38, 1, 'friend', 'male', '100002232091400', 'Fikri Fadhil Fauzan'),
(135, 'd56a219e378b60b860223d083811c33a', 38, 1, 'friend', 'female', '100002238654898', 'Eva Ulvhie'),
(136, '210b95d7889a082310384e57566466a4', 38, 1, 'friend', 'female', '100002251259049', 'Ffa Syifa'),
(137, '71dfd54c0285aba8ec84b462e7b0b07d', 38, 1, 'friend', 'female', '100002255392860', 'Wiwi C\'nouna Ribett'),
(138, 'e20c1bdbb6cff224c659a99506084db7', 38, 1, 'friend', '', '100002275620382', 'Arni Kurniati'),
(139, '4d0a91db08542b6f674e2562c124c957', 38, 1, 'friend', 'male', '100002307483409', 'Ali Tayyip Erdogann'),
(140, '3701fb40efe53ced25360ae7eea4113c', 38, 1, 'friend', 'female', '100002310351647', 'Hana Fahranaz'),
(141, 'd9d66873b305afbd188271c99b00e3f2', 38, 1, 'friend', 'female', '100002315658267', 'Laily Isnaeni'),
(142, 'aeb336515b8045d0d9b34ebd79a00dee', 38, 1, 'friend', 'female', '100002335050556', 'Ajeng Nose'),
(143, '923a56c7667ef5853cad825381c58779', 38, 1, 'friend', 'female', '100002353232332', 'Augusthine Esa ShillaNay'),
(144, '9a0c29336e084a6767c05bb1d1b1dbe9', 38, 1, 'friend', 'female', '100002381017059', 'Intan'),
(145, 'deced9561727c6fa90b0e9fa47aa545a', 38, 1, 'friend', 'female', '100002478671294', 'Rifa Habibah'),
(146, 'dd2dfdd406f5022cf552ac8eeaa10044', 38, 1, 'friend', 'female', '100002481382370', 'Cici Azhari'),
(147, '64e5b598bb1e552e569f7b2ca3c255b5', 38, 1, 'friend', 'male', '100002487586747', 'Alul Tresno'),
(148, '31d4dc4e5469068054f4edf0c15f50e9', 38, 1, 'friend', 'male', '100002491342562', 'Teguh Panca Nugraha'),
(149, '5f5d8f32f3353e53485614162f25de6f', 38, 1, 'friend', 'male', '100002517503909', 'Luqman Thariqur Rahman'),
(150, '3f2377db8a5890d35f3e96eaf744898b', 38, 1, 'friend', 'male', '100002562332977', 'Faisal Maulana PZcommunity'),
(151, '229da35fff8c50dc866f786251f66a33', 38, 1, 'friend', 'female', '100002593843724', 'Ghiffary Amanda Sas'),
(152, '4dfc8a69828aadc202f77e9e082dbf32', 38, 1, 'friend', 'female', '100002593894975', 'Nazwa'),
(153, '69c6fb35e0d1a39ad8bba354626ba9bd', 38, 1, 'friend', 'female', '100002678862668', 'Daul\'s Latif'),
(154, '054f12c4387daac3d883b758bdf83476', 38, 1, 'friend', 'male', '100002724281331', 'Abdillah Bajry'),
(155, '35569113be269f0b7fb1f3b58f268e44', 38, 1, 'friend', 'male', '100002745551490', 'Teteh Alit'),
(156, '672cf08d55573ccce79e7b6849d0c9d3', 38, 1, 'friend', 'male', '100002755034035', 'Andrea Nugraha'),
(157, 'dd7601e9f7f4f6d6378b6eb92c3d2a9b', 38, 1, 'friend', 'female', '100002757273174', 'D\'vani Alisyia RaHmani'),
(158, 'bb3a2ad803dccf7014df59dd90551e0d', 38, 1, 'friend', 'female', '100002767593595', 'Icha Aisyah'),
(159, 'f0a4c2b0793918b820f395462d27a830', 38, 1, 'friend', 'female', '100002777919425', 'Putri Tasya Ayu Lestari'),
(160, 'c4de1e1106faba0e12215746b6c41934', 38, 1, 'friend', 'male', '100002862396756', 'Ahmad Aliwildan Muhammad'),
(161, 'fa65a4be8c0aafe3261bd710313eb312', 38, 1, 'friend', 'female', '100002886499126', 'Fbi-ona Insan'),
(162, '9e763c4901a9df2ca738691c0eba8627', 38, 1, 'friend', 'male', '100002972801531', 'Yusuf Al Mujanandi'),
(163, '4a88d2ea201564f127b82b003769f236', 38, 1, 'friend', 'female', '100002978051651', 'Olla Nuraini'),
(164, 'b994522c10155956b7803f780fc2b9bb', 38, 1, 'friend', 'female', '100002995202971', 'Ratna Dewi'),
(165, '509428fe0aca9bcc34d8b8e9a9176bc9', 38, 1, 'friend', 'male', '100002996616608', 'Santri Blogger\'s'),
(166, '11cbb22f6c47a1dce086aade15ab7a71', 38, 1, 'friend', 'female', '100003003793409', 'Rani Febriani'),
(167, 'ca92328392056db7ef0ac8c35fc672ff', 38, 1, 'friend', 'female', '100003024257408', 'Gita Oktaviyani'),
(168, 'f2eac4a816934f70504c4a824c5e85a0', 38, 1, 'friend', 'female', '100003043025339', 'Alifah Rezkita Azhar'),
(169, 'd4a5d65edce37731f0b9b93c8419f63e', 38, 1, 'friend', 'female', '100003047787603', 'Suci Fathya Kania'),
(170, '694fb58480d8b0e2baeac250f00a353b', 38, 1, 'friend', 'female', '100003054523675', 'Lusi Noviani'),
(171, '76a606a2f6fb9dc6311b335bfbf83849', 38, 1, 'friend', 'female', '100003078019496', 'Salfa Mutiara'),
(172, '4f18196ab1d6d2414faf2a65766ee502', 38, 1, 'friend', 'male', '100003092817837', 'Muhammad Aditya Ghifari'),
(173, '7fc436bfd2565b5e39407397c786f7a8', 38, 1, 'friend', 'female', '100003151085286', 'Salma Fauzia Sausan'),
(174, '883834875dc98195dd00168c222010e0', 38, 1, 'friend', 'female', '100003179610947', 'Reyhan Putri'),
(175, '798fc29c66f51a2fc16dec1288dca521', 38, 1, 'friend', 'male', '100003184308180', 'Pudin Saepudin'),
(176, '4bb5a22f75acfa4d27bb3a3e35771794', 38, 1, 'friend', 'female', '100003247526209', 'Raffy'),
(177, '2191ae46788fdf56795a640d891c7008', 38, 1, 'friend', '', '100003286489893', 'Dinda Farhatun Nufus'),
(178, 'bb047e37255504d2f9162107c39d5abc', 38, 1, 'friend', 'male', '100003293692066', 'Adam Bani Nuryayi'),
(179, '5c72166fa3b71dc97106b7275b3b4790', 38, 1, 'friend', 'female', '100003330276764', 'Hizbul Khootimah Az-zaakiyyah'),
(180, '73297a660b788aeab6ab1e3d4e96753b', 38, 1, 'friend', 'male', '100003332956759', 'Iman Sulaeman'),
(181, '5923eb97270d4af9b9120d9de8d36adb', 38, 1, 'friend', 'male', '100003349126627', 'Muhammad IQball'),
(182, '22ffd476792b9e07a782aedc2e1319d9', 38, 1, 'friend', 'female', '100003352780264', 'Tia Destiyanti'),
(183, '836a5e681d25b40f8957bae612370d92', 38, 1, 'friend', 'female', '100003368854551', 'Ridha Azzahra Cii Rarassta'),
(184, '0e0492bb4a2df1216853753545d98e7b', 38, 1, 'friend', 'female', '100003385061191', 'Syifa Nurohmah'),
(185, '59c6af86e9bd37eeef5607ef88644e22', 38, 1, 'friend', 'male', '100003385713001', 'Nilla Celaloe Chaiiank Jaenal'),
(186, 'b29f2f810a082234c19be4f71132a914', 38, 1, 'friend', 'male', '100003390140902', 'Fajar Maulana'),
(187, '06d7078158ca6bf41cb1d998b68e5b6d', 38, 1, 'friend', 'male', '100003401636108', 'Yayan Mulyana'),
(188, '03aac327cffff3f947452166a1c593c8', 38, 1, 'friend', 'male', '100003405346342', 'Muhammad Wahyu Rheinaldo'),
(189, 'c33eb5aeeaabab8e3e7e9eb3292c5c25', 38, 1, 'friend', 'female', '100003463423177', 'Cha C\'bungsuArpankkboga'),
(190, 'eb459a9e8cab145b1e0fcdf120ad8808', 38, 1, 'friend', 'female', '100003474920702', 'Urfi Naufalia R'),
(191, '842aaf93c81936a82cb6ee9c1e9b31da', 38, 1, 'friend', 'female', '100003510504600', 'Muhamed Manto Dimyate'),
(192, '79e2546f16001b0a5312b703f74f2696', 38, 1, 'friend', 'female', '100003512676231', 'Ayu Suci Rahayu'),
(193, 'ce0c973e0a88a262f70feaf7fb84322a', 38, 1, 'friend', 'male', '100003539616110', 'Agus Dax Rasta Rasta'),
(194, 'a8e807032cf06ebbcc44fd9665d9f086', 38, 1, 'friend', 'male', '100003544078464', 'Muhamadz Riski Te\'a'),
(195, 'f92699dd7b39199427bbc0e077118e24', 38, 1, 'friend', 'female', '100003548984524', 'Dyanthiiee Rastafara Ygslalusayanqkamoe'),
(196, 'f0b61291d37ce2e6f099ddf8376ef310', 38, 1, 'friend', 'female', '100003554207291', 'Bee IsStarlaightpurpel'),
(197, '9df263db51bcbf37550d7776d871762a', 38, 1, 'friend', 'male', '100003570584556', 'Riskislalusayank Mam'),
(198, '19ca09ea3b6a5d610c8c583cac3208c0', 38, 1, 'friend', 'female', '100003574179094', 'Qori Nurhasanah'),
(199, '6bcc1145180d015c67f31c4445571e7c', 38, 1, 'friend', 'female', '100003581735371', 'Olla Nuraini Fadilah'),
(200, '6409dc2dc20e7558e44d0d3c25d22d43', 38, 1, 'friend', 'male', '100003589475156', 'Bex Vhierdaoez'),
(201, '26e967189282e6fa491e09e6f04516eb', 38, 1, 'friend', 'female', '100003599701786', 'Azna Nabilah Clalu Bazry'),
(202, 'e782748aea2f3b607ae31b3b07267f0d', 38, 1, 'friend', 'male', '100003604880616', 'Abdllah Zare'),
(203, 'a35c20c895b0bce0cd23d941396dc326', 38, 1, 'friend', 'female', '100003608591698', 'Misfa Yona'),
(204, '18c0b28e09898e13fa471d70c7b3c1aa', 38, 1, 'friend', 'female', '100003616151694', 'Reni Nuraeni Kuren'),
(205, '7187735d021e8f9f782755e9a59c40eb', 38, 1, 'friend', 'male', '100003636543704', 'Ahmad Fauzi'),
(206, '7c48ddea42be1b1911be672edae8c48c', 38, 1, 'friend', 'female', '100003652734228', 'Wafa Amanatul Ghina II'),
(207, '50a299376afd32549c384d52c9e457bb', 38, 1, 'friend', 'male', '100003655642536', 'アルディデビ'),
(208, '0efe772f87a00437672da7d228498051', 38, 1, 'friend', 'female', '100003660465296', 'Niaa Nurhasanah'),
(209, 'eebc3820f4df92f0a5bd453b3f80987f', 38, 1, 'friend', 'female', '100003671580969', 'Rizka Fitrianni'),
(210, '580616e17e290c41a7aa9b7b4e52127d', 38, 1, 'friend', 'male', '100003679907534', 'Muhamad Topan Mujaqi'),
(211, '1e01b0bb7555dd6e7416a077efdcaf4f', 38, 1, 'friend', 'male', '100003692349807', 'a\'Riiyu Uciihaa'),
(212, '9cf93ea4aed7a4e8721ffe92df6d6c2c', 38, 1, 'friend', 'male', '100003717741387', 'Indra Lukman Permana'),
(213, '1ba4f402d1d54c900b3d774ba07bc46a', 38, 1, 'friend', 'male', '100003731826784', 'Fauzan Ull-Quiyourra Az-Zahra'),
(214, '2462d610cf06432a216f820b17bf9da9', 38, 1, 'friend', 'female', '100003732816973', 'Dhe\'Azrii Nuraghniie Pratistha'),
(215, 'e74218db7dc4dc1cf52f152c547c1418', 38, 1, 'friend', 'female', '100003738960864', 'Cherly Putri Aini'),
(216, '1fb7dc5267e53bf59b4009c8a30e7502', 38, 1, 'friend', 'male', '100003806566178', 'Gilang Agustiana Nugraha'),
(217, '8b8c004d1453abeed61397b0783e07cd', 38, 1, 'friend', 'female', '100003823198184', 'Safira Khoirunissa'),
(218, '53304358484a542a7da46d452810e4ec', 38, 1, 'friend', 'male', '100003845107527', 'Andi Muhammad Faishal'),
(219, '919c9669a99639e8f46071d3d4805624', 38, 1, 'friend', '', '100003864022056', 'Nurul Azizah'),
(220, '54304bcfcd18369e0e5c115c4e777184', 38, 1, 'friend', 'male', '100003873021546', 'Kenny Gesa'),
(221, '7969796b8c43c104e7f598b2f6dad2f6', 38, 1, 'friend', 'male', '100003929346211', 'Feru Hardiansyah'),
(222, '412421739752b4b53dc22dcb317bdfbb', 38, 1, 'friend', 'male', '100003936607250', 'Izal Muhamad Dzulfiqor'),
(223, '76ea159f8b1fc335bfa5ef44224cc0f5', 38, 1, 'friend', 'male', '100004009074194', 'Firman Ramadhan'),
(224, 'e66a34814b58765e62d494cdfdd352b7', 38, 1, 'friend', 'female', '100004013462470', 'Elsani Istianah Kaelani'),
(225, '8fb283c8c43ef1c4dde5b9e3a468a589', 38, 1, 'friend', 'female', '100004037766433', 'Rifa Bajry'),
(226, 'ebd527a64f922dab65e42bf26ce48a8d', 38, 1, 'friend', 'female', '100004073925614', 'Miftahul Jannah'),
(227, '3d7574fc6d9483d458bde9ebd118f3bb', 38, 1, 'friend', 'male', '100004099387638', 'Yusfı Chandra'),
(228, '98730dbdaff62f15bda90bf567c498c9', 38, 1, 'friend', 'female', '100004122906960', 'Tini Supriyatin'),
(229, 'cda6c58d26ca8f0e9fd61f3b0977c07a', 38, 1, 'friend', 'female', '100004125552395', 'Ranty Nayla Sary'),
(230, '430a4aa7cbf4caddc092d6a28dda6a18', 38, 1, 'friend', 'female', '100004131973640', 'Mia Agustina'),
(231, '190e63dedf51f7d40f5f0470da9af91d', 38, 1, 'friend', 'male', '100004135120426', 'Dian M'),
(232, '5040cdb0dcc6ea821f18d980f7f6450c', 38, 1, 'friend', 'female', '100004146008759', 'Jia Apriyani'),
(233, '20487f20a2d93f225f06cefafcde16fb', 38, 1, 'friend', 'female', '100004148736630', 'Pujastuti Hoerunisa'),
(234, '052e63bf713f18e07c44808c2f15475b', 38, 1, 'friend', 'female', '100004152904456', 'Anjelli Putri'),
(235, 'd1749c670795424be76937c25a71ea70', 38, 1, 'friend', 'female', '100004206734586', 'Geclek Pizunt'),
(236, '8da6b4313087167a8b5ab7f7124f1006', 38, 1, 'friend', 'female', '100004229831709', 'Risna Al Batawi'),
(237, '24e36a32a479abd97a76f3b9e016faff', 38, 1, 'friend', 'female', '100004237266887', 'Rantika'),
(238, '0377ae86bc35d86421f10a3a3b5e7269', 38, 1, 'friend', 'female', '100004293630363', 'Raudya Tuzzahra'),
(239, '89bbc6f422adea0a48ffe4c22d64b262', 38, 1, 'friend', 'male', '100004299014555', 'Isdana Ramdansyach'),
(240, '37616c423fc54acd44539895f38a2263', 38, 1, 'friend', 'male', '100004302570040', 'Aldi Rifaldi'),
(241, '5737af9d654bd025dd33bf3fe86601e7', 38, 1, 'friend', 'male', '100004303230198', 'Deeni'),
(242, '60e382f267a1cf9c65915422a2720e53', 38, 1, 'friend', 'male', '100004359642142', 'Navis Halim'),
(243, 'a08158b7066f4c0f36146ba541ccbdda', 38, 1, 'friend', 'female', '100004422041153', 'Marisa Rahmadanti'),
(244, 'ff09b1d9fc577fa0a774023be1bfc44d', 38, 1, 'friend', 'male', '100004605393826', 'Sigit Budiarto'),
(245, 'c6d33d53497e399235a8777982cc2400', 38, 1, 'friend', 'male', '100004678323094', 'Yusfie Chandra'),
(246, '738e47b5faaa972c3afc1a1408c04692', 38, 1, 'friend', 'male', '100004689512650', 'Ahmad Wildan'),
(247, 'b54ac10b37bb335f6d35b4522f9f82bd', 38, 1, 'friend', 'female', '100004693235883', 'Rieskha Java'),
(248, 'be97dac57d71f05f262565162e497765', 38, 1, 'friend', 'female', '100004716786144', 'Asmiga Yanara'),
(249, 'abc7c5aaaabf1172d418e62d92b082a3', 38, 1, 'friend', 'female', '100004729110836', 'Wie Wie Rambe'),
(250, '85204171b1e001466da512f12124ad88', 38, 1, 'friend', 'male', '100004755938475', 'Duaempat Sepuluhhduabelass'),
(251, '9e1271f8e3933f2906d66bcdd9891285', 38, 1, 'friend', 'female', '100004762627954', 'Duaempat Sepuluhh Duaribuduabelass'),
(252, 'b6fb348cb8562dcad23054ee3542496d', 38, 1, 'friend', 'male', '100004785314194', 'Nahdi Bajri'),
(253, '67ab440de55141731cc8e9b35711822b', 38, 1, 'friend', 'female', '100004789602099', 'Adinda Purwa'),
(254, '06bad69876c0ba633109d58729dcef6f', 38, 1, 'friend', 'male', '100004836964210', 'Yadi Sopian'),
(255, '219685a2a08aee3dd0e0cb2947986084', 38, 1, 'friend', 'female', '100004852144255', 'Icha Fadhillah'),
(256, 'a43d5ffc1dd3c4ae986f9155ffd356cf', 38, 1, 'friend', 'male', '100004876299749', 'Arief AR'),
(257, '40284ac2c5c11a91eecf8c8ebe746dd3', 38, 1, 'friend', 'male', '100004878180235', 'Fauzaan'),
(258, 'c25d19cf1e216621564743ba7f8ac806', 38, 1, 'friend', 'female', '100004893005135', 'Nawa Nasa'),
(259, 'e266881584572a0262b7ab631ec9de99', 38, 1, 'friend', 'female', '100004938128736', 'Husnul Khotimah'),
(260, 'f334ee2fb38d44c2fcea5b221ece85eb', 38, 1, 'friend', 'female', '100005100963701', 'SheFaa'),
(261, '376e1ac2c004b1f7225486d9e9e0950a', 38, 1, 'friend', 'female', '100005262940615', 'Sitii Maesaroh'),
(262, '21d840a2bb7a18f20b5e64d5dc07d7eb', 38, 1, 'friend', 'female', '100005386032561', 'Selvia Rahmadani'),
(263, '0889f15c96bc0a194608c50308176432', 38, 1, 'friend', 'female', '100006047565353', 'Demoza Delieza'),
(264, '66e8c56bc4f39c3307c5ec53cbf52bbb', 38, 1, 'friend', 'female', '100006403393559', 'Imell Nazwa');

-- --------------------------------------------------------

--
-- Table structure for table `facebook_posts`
--

CREATE TABLE `facebook_posts` (
  `id` int(11) NOT NULL,
  `ids` text,
  `uid` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `category` text,
  `group` text,
  `type` text,
  `data` longtext,
  `time_post` datetime DEFAULT NULL,
  `delay` int(11) DEFAULT NULL,
  `time_delete` int(11) DEFAULT NULL,
  `result` text,
  `status` int(1) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facebook_posts`
--

INSERT INTO `facebook_posts` (`id`, `ids`, `uid`, `account`, `category`, `group`, `type`, `data`, `time_post`, `delay`, `time_delete`, `result`, `status`, `changed`, `created`) VALUES
(1, 'd2fce6be5dfeefcad300a409c0981289', 38, 1, 'group', '521758724607338', 'text', '{"media":null,"caption":"Test Cuy","link":""}', '2018-11-06 21:35:59', 0, 0, '{"message":"successfully","id":"521758724607338_1941069536009576"}', 2, '2018-11-06 21:35:59', '2018-11-06 21:35:59');

-- --------------------------------------------------------

--
-- Table structure for table `general_caption`
--

CREATE TABLE `general_caption` (
  `id` int(11) NOT NULL,
  `ids` text,
  `uid` int(11) DEFAULT NULL,
  `content` longtext,
  `status` int(1) DEFAULT '1',
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `general_coupons`
--

CREATE TABLE `general_coupons` (
  `id` int(11) NOT NULL,
  `ids` text,
  `name` text,
  `code` text,
  `type` int(1) DEFAULT '1',
  `price` float DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `packages` text,
  `status` int(1) DEFAULT '1',
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `general_file_manager`
--

CREATE TABLE `general_file_manager` (
  `id` int(11) NOT NULL,
  `ids` text CHARACTER SET utf8mb4,
  `uid` int(11) DEFAULT NULL,
  `file_name` text CHARACTER SET utf8mb4,
  `image_type` text CHARACTER SET utf8mb4,
  `file_ext` text CHARACTER SET utf8mb4,
  `file_size` text CHARACTER SET utf8mb4,
  `is_image` text CHARACTER SET utf8mb4,
  `image_width` int(11) DEFAULT NULL,
  `image_height` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_file_manager`
--

INSERT INTO `general_file_manager` (`id`, `ids`, `uid`, `file_name`, `image_type`, `file_ext`, `file_size`, `is_image`, `image_width`, `image_height`, `created`) VALUES
(1, '698cb46e00d82f555de35bed7fc209a3', 38, 'b0fe597865f19e66a7aa6c08651b5de0.png', 'image/png', 'png', '4.27', '1', 1080, 1920, '2018-11-06 21:53:12'),
(2, 'ab501a896995f3c12039df94ee00e695', 38, '6e3131683a986d4f306d6dde6a53ac16.png', 'image/png', 'png', '4.27', '1', 1080, 1920, '2018-11-06 21:53:12'),
(3, 'c74b1e4431c14fed03f7f8d8c1f88329', 38, '2ab4f010e1b34b7e41d80aec62785432.jpg', 'image/jpg', 'jpg', '89.6', '1', 1080, 1080, '2018-11-06 22:31:54'),
(4, '7856f91b2272397dbddd028902a61c02', 38, 'e38df0f6392ff8dd348ae29dd4760e42.jpg', 'image/jpg', 'jpg', '111.53', '1', 1080, 1080, '2018-11-06 22:32:39'),
(5, 'ab94b32ea9198565906f4b5da75214ac', 38, 'e32c40cf795cedeb1872d655adb77235.jpg', 'image/jpg', 'jpg', '104.26', '1', 750, 750, '2018-11-06 22:51:59');

-- --------------------------------------------------------

--
-- Table structure for table `general_lang`
--

CREATE TABLE `general_lang` (
  `id` int(11) NOT NULL,
  `ids` text NOT NULL,
  `code` text NOT NULL,
  `slug` text NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `general_lang_list`
--

CREATE TABLE `general_lang_list` (
  `id` int(11) NOT NULL,
  `ids` text,
  `name` text,
  `code` text,
  `icon` text,
  `is_default` int(1) DEFAULT '0',
  `status` int(1) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `general_options`
--

CREATE TABLE `general_options` (
  `id` int(11) NOT NULL,
  `name` text,
  `value` longtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `general_options`
--

INSERT INTO `general_options` (`id`, `name`, `value`) VALUES
(1, 'enable_https', '0'),
(2, 'website_title', 'Stackposts - Social Marketing Tool'),
(3, 'website_description', 'save time, do more, manage multiple social networks at one place'),
(4, 'website_keyword', 'social marketing tool, social planner, automation, social schedule'),
(5, 'website_favicon', 'https://social.inconsulting.tech/assets/img/favicon.png'),
(6, 'website_logo_white', 'https://social.inconsulting.tech/assets/img/logo-white.png'),
(7, 'website_logo_black', 'https://social.inconsulting.tech/assets/img/logo-black.png'),
(8, 'social_page_facebook', ''),
(9, 'social_page_google', ''),
(10, 'social_page_twitter', ''),
(11, 'social_page_instagram', ''),
(12, 'social_page_pinterest', ''),
(13, 'embed_javascript', ''),
(14, 'singup_enable', '1'),
(15, 'facebook_oauth_enable', '0'),
(16, 'google_oauth_enable', '0'),
(17, 'twitter_oauth_enable', '0'),
(18, 'google_drive_api_key', ''),
(19, 'google_drive_client_id', ''),
(20, 'website_logo_mark', 'https://social.inconsulting.tech/assets/img/logo-mark.png'),
(21, 'dropbox_api_key', ''),
(22, 'singup_verify_email_enable', '1'),
(23, 'google_oauth_client_id', ''),
(24, 'google_oauth_client_secret', ''),
(25, 'facebook_oauth_app_id', ''),
(26, 'facebook_oauth_app_secret', ''),
(27, 'twitter_oauth_client_id', ''),
(28, 'twitter_oauth_client_secret', ''),
(29, 'user_proxy', '1'),
(30, 'system_proxy', '1'),
(31, 'maximum_upload_file_size', '5'),
(32, 'email_from', ''),
(33, 'email_name', ''),
(34, 'email_protocol_type', 'mail'),
(35, 'email_smtp_server', ''),
(36, 'email_smtp_port', ''),
(37, 'email_smtp_encryption', ''),
(38, 'email_smtp_username', ''),
(39, 'email_smtp_password', ''),
(40, 'email_welcome_enable', '1'),
(41, 'email_payment_enable', '1'),
(42, 'email_activation_subject', 'Hello {full_name}! Activation your account'),
(43, 'email_activation_content', 'Welcome to {website_name}! \r\n\r\nHello {full_name},  \r\n\r\nThank you for joining! We\'re glad to have you as community member, and we\'re stocked for you to start exploring our service.  \r\n All you need to do is activate your account: \r\n  {activation_link} \r\n\r\nThanks and Best Regards!'),
(44, 'email_new_customers_subject', 'Hi {full_name}! Getting Started with Our Service'),
(45, 'email_new_customers_content', 'Hello {full_name}! \r\n\r\nCongratulations! \r\nYou have successfully signed up for our service. \r\nYou have got a trial package, starting today. \r\nWe hope you enjoy this package! We love to hear from you, \r\n\r\nThanks and Best Regards!'),
(46, 'email_forgot_password_subject', 'Hi {full_name}! Password Reset'),
(47, 'email_forgot_password_content', 'Hi {full_name}! \r\n\r\nSomebody (hopefully you) requested a new password for your account. \r\n\r\nNo changes have been made to your account yet. \r\nYou can reset your password by click this link: \r\n{recovery_password_link}. \r\n\r\nIf you did not request a password reset, no further action is required. \r\n\r\nThanks and Best Regards!'),
(48, 'email_renewal_reminders_subject', 'Hi {full_name}, Here\'s a little Reminder your Membership is expiring soon...'),
(49, 'email_renewal_reminders_content', 'Dear {full_name}, \r\n\r\nYour membership with your current package will expire in {days_left} days. \r\nWe hope that you will take the time to renew your membership and remain part of our community. It couldn’t be easier - just click here to renew: {website_link} \r\n\r\nThanks and Best Regards!'),
(50, 'email_payment_subject', 'Hi {full_name}, Thank you for your payment'),
(51, 'email_payment_content', 'Hi {full_name}, \r\n\r\nYou just completed the payment successfully on our service. \r\nThank you for being awesome, we hope you enjoy your package. \r\n\r\nThanks and Best Regards!'),
(52, 'fb_min_post_interval_seconds', '50'),
(53, 'fb_post_auto_pause_after_post', '50'),
(54, 'fb_post_auto_resume_after_minute_hours', '50'),
(55, 'fb_post_repeat_frequency', '50'),
(56, 'instagram_verify_code_enable', '1'),
(57, 'enable_advance_option', '1'),
(58, 'twitter_app_id', ''),
(59, 'twitter_app_secret', ''),
(60, 'linkedin_app_id', ''),
(61, 'linkedin_app_secret', '');

-- --------------------------------------------------------

--
-- Table structure for table `general_packages`
--

CREATE TABLE `general_packages` (
  `id` int(11) NOT NULL,
  `ids` text,
  `type` int(1) DEFAULT '1' COMMENT '1-TRIAL|2-CHARGE',
  `name` text,
  `description` text,
  `price_monthly` float DEFAULT NULL,
  `price_annually` float DEFAULT NULL,
  `number_accounts` int(11) DEFAULT '0',
  `is_default` int(1) DEFAULT '0',
  `trial_day` int(11) DEFAULT NULL,
  `permission` longtext,
  `sort` int(11) DEFAULT '1',
  `status` int(1) DEFAULT '1',
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `general_packages`
--

INSERT INTO `general_packages` (`id`, `ids`, `type`, `name`, `description`, `price_monthly`, `price_annually`, `number_accounts`, `is_default`, `trial_day`, `permission`, `sort`, `status`, `changed`, `created`) VALUES
(1, 'c761441297cac88f7cea247f260d1985', 1, 'Trial mode', NULL, 0, 0, 0, 0, 3, '{"0":"facebook_enable","1":"facebook\\/post","2":"instagram_enable","3":"instagram\\/post","4":"twitter_enable","5":"twitter\\/post","6":"google_drive","7":"dropbox","8":"photo_type","9":"video_type","max_storage_size":100,"max_file_size":20}', 0, 1, '2018-06-11 19:21:26', '2018-04-02 11:40:23'),
(2, 'd7394fc22455c18ee2eb177bacb0a082', 2, 'Basic', 'Pick great plan for you', 30, 25, 1, 0, NULL, '{"0":"facebook_enable","1":"facebook\\/post","2":"instagram_enable","3":"instagram\\/post","4":"twitter_enable","5":"twitter\\/post","6":"google_drive","7":"dropbox","8":"photo_type","9":"video_type","max_storage_size":250,"max_file_size":2}', 60, 1, '2018-07-26 09:29:34', '2018-04-02 11:40:28'),
(8, '2c327cb5ab20f86cc0ea9cae47515da1', 2, 'Standard', 'Pick great plan for you', 60, 50, 2, 0, NULL, '{"0":"facebook_enable","1":"facebook\\/post","2":"instagram_enable","3":"instagram\\/post","4":"google_drive","5":"dropbox","6":"photo_type","7":"video_type","max_storage_size":100,"max_file_size":5}', 80, 1, '2018-07-26 09:29:23', '2018-06-09 19:58:16'),
(9, '9088ff7e41e726e5e2bf7c1352c22340', 2, 'Premium', 'Pick great plan for you', 0, 0, 5, 0, NULL, '{"0":"facebook_enable","1":"facebook\\/post","2":"instagram_enable","3":"instagram\\/post","4":"twitter_enable","5":"twitter\\/post","6":"google_drive","7":"dropbox","8":"photo_type","9":"video_type","max_storage_size":1000,"max_file_size":20}', 0, 1, '2018-09-29 09:24:16', '2018-07-26 08:51:38');

-- --------------------------------------------------------

--
-- Table structure for table `general_payment_history`
--

CREATE TABLE `general_payment_history` (
  `id` int(11) NOT NULL,
  `ids` text,
  `uid` int(11) DEFAULT NULL,
  `package` int(11) DEFAULT NULL,
  `type` text,
  `transaction_id` text,
  `plan` int(1) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `general_proxies`
--

CREATE TABLE `general_proxies` (
  `id` int(11) NOT NULL,
  `ids` text,
  `address` text,
  `location` text,
  `status` int(1) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `general_proxies`
--

INSERT INTO `general_proxies` (`id`, `ids`, `address`, `location`, `status`, `changed`, `created`) VALUES
(1, '2960cc1e304430dd4a5c16c02a2ab078', 'Bandung', 'ID', 1, '2018-11-06 21:37:22', '2018-11-06 21:37:22');

-- --------------------------------------------------------

--
-- Table structure for table `general_purchase`
--

CREATE TABLE `general_purchase` (
  `id` int(11) NOT NULL,
  `ids` text,
  `pid` text,
  `purchase_code` text,
  `version` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `general_purchase`
--

INSERT INTO `general_purchase` (`id`, `ids`, `pid`, `purchase_code`, `version`) VALUES
(2, 'c915a0fba1edb89d27c2b4f6e68b9504', '1', 'GPLed by g0g0', '1.5'),
(3, 'be61ecaeaf80e4635384c3046a45f199', '1', 'GPLed by g0g0', '1.5');

-- --------------------------------------------------------

--
-- Table structure for table `general_sessions`
--

CREATE TABLE `general_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_sessions`
--

INSERT INTO `general_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('u8arahmf6t6vhou1uv1g6onhqfu9kouu', '180.245.189.78', 1541516213, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531363231333b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b69675f726b666f72707265736964656e5f63737266746f6b656e7c733a33323a227841616236314a45443444457130654f6f504a45494b597a5933413549656e46223b),
('575go5evp9ubm5oiss387rl6qgp4g31j', '180.245.189.78', 1541515897, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531353839373b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b69675f726b666f72707265736964656e5f63737266746f6b656e7c733a33323a227841616236314a45443444457130654f6f504a45494b597a5933413549656e46223b),
('obm7fksh2t9v0adt95p60i1g41afubc7', '127.0.0.1', 1541515014, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531353031343b6c616e675f64656661756c747c733a343a226e756c6c223b),
('04lsghq3vonpf4g9skcf1rpdvi34sdgm', '127.0.0.1', 1541515014, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531353031343b6c616e675f64656661756c747c733a343a226e756c6c223b),
('f61pof0v0r7igiiql4uvdsojup2uf0r8', '127.0.0.1', 1541515014, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531353031343b),
('pecmafrbdt0np3peh06t5afrsvcbmhev', '127.0.0.1', 1541515014, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531353031343b6c616e675f64656661756c747c733a343a226e756c6c223b),
('3vjgaotm5khnt35s5l350cdrn9v7jjqo', '180.245.189.78', 1541514902, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531343930323b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b),
('uf501msic7tgeoapdgea443u8a3uc4hg', '180.245.189.78', 1541515293, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531353239333b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b),
('7kumfmoh0t4i9va6h25savti9ank0ane', '180.245.189.78', 1541516653, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531363635333b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b69675f726b666f72707265736964656e5f63737266746f6b656e7c733a33323a227841616236314a45443444457130654f6f504a45494b597a5933413549656e46223b),
('d2ktm7c1en1usquctthh0j5g03iegfov', '180.245.189.78', 1541518265, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531383236353b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b69675f726b666f72707265736964656e5f63737266746f6b656e7c733a33323a227841616236314a45443444457130654f6f504a45494b597a5933413549656e46223b),
('2ejuf9adqnfqihsuln750rmejqgebu67', '180.245.189.78', 1541518578, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531383537383b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b69675f726b666f72707265736964656e5f63737266746f6b656e7c733a33323a227841616236314a45443444457130654f6f504a45494b597a5933413549656e46223b),
('d8v1jn4s8d8jn2geh4bmpifruhs26fhv', '180.245.189.78', 1541518917, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531383931373b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b69675f726b666f72707265736964656e5f63737266746f6b656e7c733a33323a227841616236314a45443444457130654f6f504a45494b597a5933413549656e46223b),
('a43dpq0fh749g1kpbd5tgesevnhkcad1', '180.245.189.78', 1541519235, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393233353b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b69675f726b666f72707265736964656e5f63737266746f6b656e7c733a33323a227841616236314a45443444457130654f6f504a45494b597a5933413549656e46223b),
('r8tllahs0bv3i4ptrt45hng1phjf8og4', '180.245.189.78', 1541519730, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393733303b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b69675f726b666f72707265736964656e5f63737266746f6b656e7c733a33323a227841616236314a45443444457130654f6f504a45494b597a5933413549656e46223b),
('7jaqt0lhol1bthmq5gsc2104vq4l5j2j', '127.0.0.1', 1541519441, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393434313b),
('tfekj15a50ff4co3ndrrnokn1hepu7oq', '127.0.0.1', 1541519441, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393434313b6c616e675f64656661756c747c733a343a226e756c6c223b),
('6kid36k9443lif3agqfinte271unsi2t', '127.0.0.1', 1541519441, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393434313b6c616e675f64656661756c747c733a343a226e756c6c223b),
('stg1cpr0g41shv83l1vepl2u0invonsv', '127.0.0.1', 1541519441, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393434313b6c616e675f64656661756c747c733a343a226e756c6c223b),
('aqlulka2b05pkc2kaefnjvk9r8pi4baf', '127.0.0.1', 1541519444, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393434343b),
('c67dbn66o926g73i21sehtb5pfveprd8', '127.0.0.1', 1541519444, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393434343b6c616e675f64656661756c747c733a343a226e756c6c223b),
('25gc12liglafi59dsrfskqhsnimuo10j', '127.0.0.1', 1541519444, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393434343b6c616e675f64656661756c747c733a343a226e756c6c223b),
('3qqcepe0c4rrgn51mnmusjbv6hb2qtpj', '127.0.0.1', 1541519444, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393434343b6c616e675f64656661756c747c733a343a226e756c6c223b),
('i452il8bafcc5fqcd8tq3lnnoiqva3mt', '180.245.189.78', 1541520483, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313532303438333b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b69675f726b666f72707265736964656e5f63737266746f6b656e7c733a33323a227841616236314a45443444457130654f6f504a45494b597a5933413549656e46223b),
('0979va00m60sobofn4bgmtjdc6eoqlhv', '127.0.0.1', 1541519745, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393734353b),
('df2nrfqblnhh341ae4jg7a6mrp7qihgl', '127.0.0.1', 1541519745, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393734353b6c616e675f64656661756c747c733a343a226e756c6c223b),
('uqglb1ffuedndcfv2jg3um7lm5aj2hja', '127.0.0.1', 1541519745, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393734353b6c616e675f64656661756c747c733a343a226e756c6c223b),
('3dvgelbf5fkrdvqugfafmv1ghva0uu5f', '127.0.0.1', 1541519745, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313531393734353b6c616e675f64656661756c747c733a343a226e756c6c223b),
('9obshtq8fs470608jqf2mst5trt3ejur', '180.245.189.78', 1541520595, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313532303438333b6c616e675f64656661756c747c733a343a226e756c6c223b7569647c733a323a223338223b746d705f6b6f73746c61622e69645f636f756e747c693a393b69675f6b6f73746c61622e69645f63737266746f6b656e7c4e3b69675f726b666f72707265736964656e5f63737266746f6b656e7c733a33323a227841616236314a45443444457130654f6f504a45494b597a5933413549656e46223b),
('n57f5kgjsrdja01du470jj2aurulfksm', '125.165.239.212', 1541590823, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313539303832333b6c616e675f64656661756c747c733a343a226e756c6c223b),
('mqb989vkgjkni7u4ep6q7k9rrhhfs7e4', '125.165.239.212', 1541770200, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313737303230303b6c616e675f64656661756c747c733a343a226e756c6c223b),
('todn8uo43j4genehll6oa7420ak54f94', '125.165.239.212', 1541770216, 0x5f5f63695f6c6173745f726567656e65726174657c693a313534313737303230303b6c616e675f64656661756c747c733a343a226e756c6c223b);

-- --------------------------------------------------------

--
-- Table structure for table `general_users`
--

CREATE TABLE `general_users` (
  `id` int(11) NOT NULL,
  `ids` text,
  `admin` int(1) DEFAULT NULL,
  `login_type` text,
  `fullname` text,
  `email` text,
  `password` text,
  `package` int(11) DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `timezone` text,
  `permission` text,
  `settings` longtext,
  `activation_key` text,
  `reset_key` text,
  `history_ip` text,
  `status` int(1) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `general_users`
--

INSERT INTO `general_users` (`id`, `ids`, `admin`, `login_type`, `fullname`, `email`, `password`, `package`, `expiration_date`, `timezone`, `permission`, `settings`, `activation_key`, `reset_key`, `history_ip`, `status`, `changed`, `created`) VALUES
(1, 'c761441297cac88f7cea247f260d1985', 1, 'direct', 'Rohit Sharma', 'rohit@interstellarconsulting.com', '1db2cd81f19741d67e4c7aef245a689e', 2, '2025-01-01', 'Pacific/Niue', NULL, '{"fb_post_media_count":0}', '1', 'b04e3f63724775f74a561648907b70de', '["46.223.102.154","46.223.102.154","46.223.102.154"]', 1, '2018-07-26 16:28:40', '2018-07-26 13:39:32'),
(38, '3a40208971d1baf6152fc14fb4cc7bbe', 1, 'direct', 'Fata El Islami', 'fataelislami97@gmail.com', '13c4689beae8cabf2d378d6b8631c21f', 1, '2018-11-09', 'Asia/Jakarta', '{"0":"facebook_enable","1":"facebook\\/post","2":"instagram_enable","3":"instagram\\/post","4":"twitter_enable","5":"twitter\\/post","6":"google_drive","7":"dropbox","8":"photo_type","9":"video_type","max_storage_size":100,"max_file_size":20}', '{"fb_post_media_count":0,"fb_post_link_count":0,"fb_post_text_count":1,"fb_post_success_count":1,"fb_post_error_count":0,"linkedin_app_id":"","linkedin_app_secret":"","twitter_app_id":"","twitter_app_secret":"","fb_post_count":1,"ig_post_photo_count":1,"ig_post_story_count":1,"ig_post_carousel_count":0,"ig_post_success_count":2,"ig_post_error_count":0,"ig_post_count":2,"linkedin_post_photo_count":0,"linkedin_post_link_count":0,"linkedin_post_success_count":0,"linkedin_post_error_count":0}', '1', '7e9205cdc8296284deaa7aabb7ca6e83', '["180.245.189.78"]', 1, '2018-11-06 21:22:25', '2018-11-06 21:22:25');

-- --------------------------------------------------------

--
-- Table structure for table `instagram_accounts`
--

CREATE TABLE `instagram_accounts` (
  `id` int(11) NOT NULL,
  `ids` text CHARACTER SET utf8,
  `uid` text,
  `pid` text,
  `avatar` text,
  `username` text,
  `password` text,
  `proxy` text,
  `default_proxy` int(11) DEFAULT '0',
  `status` int(1) DEFAULT '1',
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `instagram_accounts`
--

INSERT INTO `instagram_accounts` (`id`, `ids`, `uid`, `pid`, `avatar`, `username`, `password`, `proxy`, `default_proxy`, `status`, `changed`, `created`) VALUES
(1, '7fbba1c19635a4552af916ba61d7310e', '38', '9025869623', 'https://avatars.io/instagram/rkforpresiden', 'rkforpresiden', '5ec9b146aa3a210218cecd7c25d39ad8afa57103cfde42f6fa96e973ff665dd6c100582a38a6679414069a643fc744b477d028a14e23eec59ef108dfab55c588dDyobRel8gBR5p7mT1C7EsPKD3+AAcKUVnDB6gs5wDk=', 'http://rkforpresiden:bismillah100%@119.2.41.98:36953', 1, 1, '2018-11-06 21:50:55', '2018-11-06 21:50:55');

-- --------------------------------------------------------

--
-- Table structure for table `instagram_posts`
--

CREATE TABLE `instagram_posts` (
  `id` int(11) NOT NULL,
  `ids` text,
  `uid` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `type` text,
  `data` longtext,
  `time_post` datetime DEFAULT NULL,
  `delay` int(11) DEFAULT NULL,
  `time_delete` int(11) DEFAULT NULL,
  `result` text,
  `status` int(1) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `instagram_posts`
--

INSERT INTO `instagram_posts` (`id`, `ids`, `uid`, `account`, `type`, `data`, `time_post`, `delay`, `time_delete`, `result`, `status`, `changed`, `created`) VALUES
(1, 'f0388865003c1e36b7e5e3876e1dd96b', 38, 1, 'story', '{"media":["https:\\/\\/islamify.id\\/project\\/stackpost\\/assets\\/uploads\\/user38\\/b0fe597865f19e66a7aa6c08651b5de0.png"],"caption":"Hey","comment":""}', '2018-11-06 21:53:24', 0, 0, '1906696997772006386', 2, '2018-11-06 21:53:24', '2018-11-06 21:53:24'),
(2, 'ddba1ff7a2eef5b805906224dbc7adcb', 38, 1, 'photo', '{"media":["https:\\/\\/islamify.id\\/project\\/stackpost\\/assets\\/uploads\\/user38\\/e32c40cf795cedeb1872d655adb77235.jpg"],"caption":"Coba colek pacarnya yg hobi pacaran pake sendal \\ud83d\\ude1b\\r\\nCredit: @mensociete","comment":""}', '2018-11-06 22:52:27', 0, 0, '1906726723467231665', 2, '2018-11-06 22:52:27', '2018-11-06 22:52:27');

-- --------------------------------------------------------

--
-- Table structure for table `instagram_sessions`
--

CREATE TABLE `instagram_sessions` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `settings` mediumblob,
  `cookies` mediumblob,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `instagram_sessions`
--

INSERT INTO `instagram_sessions` (`id`, `username`, `settings`, `cookies`, `last_modified`) VALUES
(1, 'kostlab.id', 0x7b22646576696365737472696e67223a2232335c2f362e302e313b203634306470693b203134343078323536303b2073616d73756e673b20534d2d47393330463b206865726f6c74653b2073616d73756e676578796e6f7338383930222c226465766963655f6964223a22616e64726f69642d32613064346466633539653363383239222c2270686f6e655f6964223a2265653533616163662d396436382d346566312d393762612d313064313933646465393734222c2275756964223a2264306539626134332d316539612d346439352d396332612d623031656536343165323666222c226164766572746973696e675f6964223a2265376338386231372d636139662d343165632d393563342d393761356432343561373533222c2273657373696f6e5f6964223a2230303431393134342d313238622d343937382d386236362d303035323435663330346531222c226578706572696d656e7473223a22222c2266626e735f61757468223a22222c2266626e735f746f6b656e223a22222c226c6173745f66626e735f746f6b656e223a22222c226c6173745f6c6f67696e223a22222c226c6173745f6578706572696d656e7473223a22222c226461746163656e746572223a22222c2270726573656e63655f64697361626c6564223a22222c227a725f746f6b656e223a22222c227a725f65787069726573223a22222c227a725f72756c6573223a22222c226163636f756e745f6964223a22227d, 0x5b5d, '2018-11-06 14:27:01'),
(2, 'rkforpresiden', 0x7b22646576696365737472696e67223a2232335c2f362e302e313b203634306470693b203134343078323536303b2073616d73756e673b20534d2d47393335463b206865726f326c74653b2073616d73756e676578796e6f7338383930222c226465766963655f6964223a22616e64726f69642d31663364373065616465303661653239222c2270686f6e655f6964223a2237643138326234342d363864632d346237342d386663612d376631616231646436626265222c2275756964223a2234313862356635362d626438322d346534662d613366662d363465366632393630306366222c226164766572746973696e675f6964223a2239373334663137372d373536652d343131662d623061322d323365623063396462383666222c2273657373696f6e5f6964223a2266356139613863642d646637352d346664342d623430642d313766393865663830613963222c226578706572696d656e7473223a225a654e71566b304679677a414d52655c2f6964525a746c376d4d787469436148416b4b7074514a73506461314c436b454a43757750385c2f7266304a6136474b72447356636a4452326d644f56344e52554332525542766a75622b4e427957614b7a377a6f596146514a64455043436e43416d71796c4c5c2f63726b5c2f5a65382b677752557438515630447379646b6b757174716d79445767324967573143673145504c2b58714e2b4b69467370685954346f7556336179696a664877795a5753676a535a614e6438747947524f434579317a594871794934525c2f51324f68636351366b3339582b6a5771355a756c344939486d4a456d57344f7330625c2f69477a595538436b53737a6e6b4e634237554537754a6d7a724e646d2b5064645c2f50353647735737736a61765069505863596f333139656f74387832555a3338484d58333936374554487679436446504f626b356254526b445455496b4c2b594a474d534937584f31373068624e4d487744376c425447513d3d222c2266626e735f61757468223a22222c2266626e735f746f6b656e223a22222c226c6173745f66626e735f746f6b656e223a22222c226c6173745f6c6f67696e223a2231353431353138323939222c226c6173745f6578706572696d656e7473223a2231353431353135383733222c226461746163656e746572223a22222c2270726573656e63655f64697361626c6564223a22222c227a725f746f6b656e223a22222c227a725f65787069726573223a2231353431353837383832222c227a725f72756c6573223a224a5b5d222c226163636f756e745f6964223a2239303235383639363233227d, 0x5b7b224e616d65223a22727572222c2256616c7565223a22465243222c22446f6d61696e223a222e696e7374616772616d2e636f6d222c2250617468223a225c2f222c224d61782d416765223a6e756c6c2c2245787069726573223a6e756c6c2c22536563757265223a747275652c2244697363617264223a66616c73652c22487474704f6e6c79223a747275657d2c7b224e616d65223a226d6964222c2256616c7565223a22572d477156414142414146705739744a66304153436d59797847646c222c22446f6d61696e223a222e696e7374616772616d2e636f6d222c2250617468223a225c2f222c224d61782d416765223a22333135333630303030222c2245787069726573223a313835363837353836302c22536563757265223a747275652c2244697363617264223a66616c73652c22487474704f6e6c79223a66616c73657d2c7b224e616d65223a226d6364222c2256616c7565223a2233222c22446f6d61696e223a222e696e7374616772616d2e636f6d222c2250617468223a225c2f222c224d61782d416765223a22333135333630303030222c2245787069726573223a313835363837353836302c22536563757265223a747275652c2244697363617264223a66616c73652c22487474704f6e6c79223a66616c73657d2c7b224e616d65223a2264735f75736572222c2256616c7565223a22726b666f72707265736964656e222c22446f6d61696e223a222e696e7374616772616d2e636f6d222c2250617468223a225c2f222c224d61782d416765223a2237373736303030222c2245787069726573223a313534393239313836392c22536563757265223a747275652c2244697363617264223a66616c73652c22487474704f6e6c79223a747275657d2c7b224e616d65223a226967666c222c2256616c7565223a22726b666f72707265736964656e222c22446f6d61696e223a222e696e7374616772616d2e636f6d222c2250617468223a225c2f222c224d61782d416765223a223836343030222c2245787069726573223a313534313630323237352c22536563757265223a747275652c2244697363617264223a66616c73652c22487474704f6e6c79223a747275657d2c7b224e616d65223a2269675f6469726563745f726567696f6e5f68696e74222c2256616c7565223a2250524e222c22446f6d61696e223a222e696e7374616772616d2e636f6d222c2250617468223a225c2f222c224d61782d416765223a22363034383030222c2245787069726573223a313534323132333130322c22536563757265223a747275652c2244697363617264223a66616c73652c22487474704f6e6c79223a747275657d2c7b224e616d65223a2269735f737461727265645f656e61626c6564222c2256616c7565223a22796573222c22446f6d61696e223a222e696e7374616772616d2e636f6d222c2250617468223a225c2f222c224d61782d416765223a22333135333630303030222c2245787069726573223a313835363837393535322c22536563757265223a747275652c2244697363617264223a66616c73652c22487474704f6e6c79223a747275657d2c7b224e616d65223a2264735f757365725f6964222c2256616c7565223a2239303235383639363233222c22446f6d61696e223a222e696e7374616772616d2e636f6d222c2250617468223a225c2f222c224d61782d416765223a2237373736303030222c2245787069726573223a313534393239353536322c22536563757265223a747275652c2244697363617264223a66616c73652c22487474704f6e6c79223a66616c73657d2c7b224e616d65223a2275726c67656e222c2256616c7565223a225c227b5c5c5c223131392e322e34312e39385c5c5c223a2033383532347d3a31674b33656b3a6c76686e4567713758643164473639386f7258645f4e5a4e7169345c22222c22446f6d61696e223a222e696e7374616772616d2e636f6d222c2250617468223a225c2f222c224d61782d416765223a6e756c6c2c2245787069726573223a6e756c6c2c22536563757265223a747275652c2244697363617264223a66616c73652c22487474704f6e6c79223a747275657d2c7b224e616d65223a2273657373696f6e6964222c2256616c7565223a22393032353836393632332533413941494c666165757938554a45742533413236222c22446f6d61696e223a222e696e7374616772616d2e636f6d222c2250617468223a225c2f222c224d61782d416765223a223331353336303030222c2245787069726573223a313537333035353536322c22536563757265223a747275652c2244697363617264223a66616c73652c22487474704f6e6c79223a747275657d2c7b224e616d65223a2263737266746f6b656e222c2256616c7565223a22395861707a58663969664345424455584d716d4c3871777439684b7551493874222c22446f6d61696e223a222e696e7374616772616d2e636f6d222c2250617468223a225c2f222c224d61782d416765223a223331343439363030222c2245787069726573223a313537323936393136322c22536563757265223a747275652c2244697363617264223a66616c73652c22487474704f6e6c79223a66616c73657d5d, '2018-11-06 15:52:42');

-- --------------------------------------------------------

--
-- Table structure for table `linkedin_accounts`
--

CREATE TABLE `linkedin_accounts` (
  `id` int(11) NOT NULL,
  `ids` text,
  `uid` int(11) DEFAULT NULL,
  `pid` text,
  `type` text,
  `username` text,
  `url` text,
  `avatar` text,
  `access_token` text,
  `status` int(1) DEFAULT '1',
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `linkedin_posts`
--

CREATE TABLE `linkedin_posts` (
  `id` int(11) NOT NULL,
  `ids` text,
  `uid` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `type` text,
  `data` longtext,
  `time_post` datetime DEFAULT NULL,
  `delay` int(11) DEFAULT NULL,
  `time_delete` int(11) DEFAULT NULL,
  `result` text,
  `status` int(1) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `twitter_accounts`
--

CREATE TABLE `twitter_accounts` (
  `id` int(11) NOT NULL,
  `ids` text,
  `uid` int(11) DEFAULT NULL,
  `pid` text,
  `username` text,
  `avatar` text,
  `access_token` text,
  `status` int(1) DEFAULT '1',
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `twitter_posts`
--

CREATE TABLE `twitter_posts` (
  `id` int(11) NOT NULL,
  `ids` text,
  `uid` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `type` text,
  `data` longtext,
  `time_post` datetime DEFAULT NULL,
  `delay` int(11) DEFAULT NULL,
  `time_delete` int(11) DEFAULT NULL,
  `result` text,
  `status` int(1) DEFAULT NULL,
  `changed` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `facebook_accounts`
--
ALTER TABLE `facebook_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facebook_groups`
--
ALTER TABLE `facebook_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facebook_posts`
--
ALTER TABLE `facebook_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_caption`
--
ALTER TABLE `general_caption`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_coupons`
--
ALTER TABLE `general_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_file_manager`
--
ALTER TABLE `general_file_manager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_lang`
--
ALTER TABLE `general_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_lang_list`
--
ALTER TABLE `general_lang_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_options`
--
ALTER TABLE `general_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_packages`
--
ALTER TABLE `general_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_payment_history`
--
ALTER TABLE `general_payment_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_proxies`
--
ALTER TABLE `general_proxies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_purchase`
--
ALTER TABLE `general_purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_sessions`
--
ALTER TABLE `general_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `general_users`
--
ALTER TABLE `general_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instagram_accounts`
--
ALTER TABLE `instagram_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instagram_posts`
--
ALTER TABLE `instagram_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instagram_sessions`
--
ALTER TABLE `instagram_sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `linkedin_accounts`
--
ALTER TABLE `linkedin_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `linkedin_posts`
--
ALTER TABLE `linkedin_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twitter_posts`
--
ALTER TABLE `twitter_posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `facebook_accounts`
--
ALTER TABLE `facebook_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `facebook_groups`
--
ALTER TABLE `facebook_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=265;
--
-- AUTO_INCREMENT for table `facebook_posts`
--
ALTER TABLE `facebook_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `general_caption`
--
ALTER TABLE `general_caption`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_coupons`
--
ALTER TABLE `general_coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_file_manager`
--
ALTER TABLE `general_file_manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `general_lang`
--
ALTER TABLE `general_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_lang_list`
--
ALTER TABLE `general_lang_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_options`
--
ALTER TABLE `general_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `general_packages`
--
ALTER TABLE `general_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `general_payment_history`
--
ALTER TABLE `general_payment_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general_proxies`
--
ALTER TABLE `general_proxies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `general_purchase`
--
ALTER TABLE `general_purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `general_users`
--
ALTER TABLE `general_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `instagram_accounts`
--
ALTER TABLE `instagram_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `instagram_posts`
--
ALTER TABLE `instagram_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `instagram_sessions`
--
ALTER TABLE `instagram_sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `linkedin_accounts`
--
ALTER TABLE `linkedin_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `linkedin_posts`
--
ALTER TABLE `linkedin_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `twitter_posts`
--
ALTER TABLE `twitter_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
